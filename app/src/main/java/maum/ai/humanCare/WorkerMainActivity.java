package maum.ai.humanCare;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.internal.LinkedTreeMap;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkerMainActivity extends AppCompatActivity {

    String categoryId = "";

    AlertDialog mDlg_Logout = null;
    AlertDialog.Builder builder;
    AlertDialog mDlg_reqProject = null;
    AlertDialog.Builder builder1;

    ArrayAdapter<String> adapter;

    @BindView(R.id.btnLogout)
    Button btn_logout;

    @BindView(R.id.btnRequestProject)
    Button btn_requestProject;

    @BindView(R.id.workList)
    LinearLayout layout_workList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.worker_main);

        ButterKnife.bind(this);

        builder = new AlertDialog.Builder(this);
        final EditText et = new EditText(this);
        builder1 = new AlertDialog.Builder(this);
        final EditText et1 = new EditText(this);

        getMyInfo();

        getMyProjectList();

        onBackPressed();

        // '프로젝트 할당 요청' 버튼
        btn_requestProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                builder1.setView(et1);
                et1.setHint("비밀번호 입력");

                builder1.setTitle("프로젝트 할당 확인")
                        .setMessage("프로젝트 할당을 위해 비밀번호를 입력해주세요")
                        .setCancelable(false)
                        .setPositiveButton("프로젝트 할당", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String pwValue = et1.getText().toString();

                                if (pwValue.equals("human1234") == true) {
                                    Log.d("apiTest", "------------------------------------------------- 프로젝트 할당 비밀번호 입력 성공");
                                    createCategoryList();
                                    et1.setText("");
                                } else {
                                    Toast.makeText(getApplicationContext(), "비밀번호가 일치하지 않습니다", Toast.LENGTH_LONG).show();
                                    et1.setText("");
                                }
                            }
                        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                if (mDlg_reqProject == null) mDlg_reqProject = builder1.create();
                mDlg_reqProject.show();
            }
        });

        // '로그아웃' 버튼
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                builder.setView(et);
                et.setHint("비밀번호 입력");

                builder.setTitle("로그아웃 확인")
                        .setMessage("로그아웃을 위해 비밀번호를 입력해주세요")
                        .setCancelable(false)
                        .setPositiveButton("로그아웃", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String pwValue = et.getText().toString();

                                if (pwValue.equals("human1234") == true) {
                                    Log.d("apiTest", "------------------------------------------------- 로그인 성공");
                                    //로그아웃 API
                                    Call<HashMap<String, Object>> callLogoutResult = MainActivity.service.getLogoutResult();
                                    callLogoutResult.enqueue(new Callback<HashMap<String, Object>>() {
                                        @Override
                                        public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                                            if (response.isSuccessful()) {
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            } else {
                                                Toast.makeText(getApplicationContext(), "서버와의 통신이 원활하지 않습니다", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                                            Log.e("apiTest", t.getMessage());
                                        }
                                    });
                                } else {
                                    Toast.makeText(getApplicationContext(), "비밀번호가 일치하지 않습니다", Toast.LENGTH_LONG).show();
                                    et.setText("");
                                }
                            }
                        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                if (mDlg_Logout == null) mDlg_Logout = builder.create();
                mDlg_Logout.show();
            }
        });
    }

    private void getMyInfo() {
        Call<HashMap<String, Object>> callUserInfo = MainActivity.service.getUserId();
        callUserInfo.enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                if (response.isSuccessful()) {
                    // userId에 맞추어 버튼 이름 변경
                    btn_logout.setText(((LinkedTreeMap<String, Object>) response.body().get("data")).get("userId").toString());
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "계정 정보를 받아올 수 없습니다", Toast.LENGTH_LONG).show();
                Log.d("apiTest", "" + t.getMessage());
            }
        });
    }

    private void getMyProjectList() {
        Call<HashMap<String, Object>> callMyProject = MainActivity.service.getMyProjectList();
        callMyProject.enqueue(new Callback<HashMap<String, Object>>() {

            List<LinkedTreeMap<String, Object>> projectListData;

            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {

                View.OnClickListener projectListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String projectId = v.getTag().toString();

                        Button b = (Button)v;
                        String projectName = b.getText().toString();

                        Intent intent = new Intent(getApplicationContext(), WorkerRecordActivity.class);
                        intent.putExtra("projectId", ""+projectId);
                        intent.putExtra("projectName", ""+projectName);
                        //
                        intent.putExtra("userId", ""+btn_logout.getText());

                        startActivity(intent);
                    }
                };

                if (response.isSuccessful()) {
                    layout_workList.removeAllViews();
                    projectListData = (List<LinkedTreeMap<String, Object>>) response.body().get("data");

                    for (int i = 0; i < projectListData.size(); i++) {
                        Button tempBtn = new Button(getApplicationContext());
                        tempBtn.setText(projectListData.get(i).get("projectNm").toString());
                        tempBtn.setTextSize(50);
                        tempBtn.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 230));
                        tempBtn.setTag(projectListData.get(i).get("projectId"));
                        tempBtn.setOnClickListener(projectListener);

                        layout_workList.addView(tempBtn);
                    }
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "프로젝트 목록을 받아올 수 없습니다", Toast.LENGTH_LONG).show();
                Log.d("apiTest", "" + t.getMessage());
            }
        });
    }

    private void createCategoryList() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("카테고리 목록");
        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_expandable_list_item_1);
        alert.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String menu = adapter.getItem(which);
                menu = adapter.getItem(which).substring(menu.indexOf("_") + 1, menu.length());
                categoryId = menu;

                reqProject();
            }
        });

        Call<HashMap<String, Object>> callProjectCategory = MainActivity.service.getProjectCategory("PJT_CTG");
        callProjectCategory.enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                if (response.isSuccessful()) {

                    List<LinkedTreeMap<String, Object>> listData = (List<LinkedTreeMap<String, Object>>) response.body().get("data");
                    adapter.add("임의선택_ ");
                    for (int i = 0; i < listData.size(); i++) {
                        adapter.add("" + listData.get(i).get("codeName") + "_" + listData.get(i).get("code"));
                    }
                    adapter.notifyDataSetChanged();
                    alert.show();
                } else {
                    Toast.makeText(getApplicationContext(), "서버와의 통신이 원활하지 않습니다", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "프로젝트 목록을 받아올 수 없습니다", Toast.LENGTH_LONG).show();
                Log.e("apiTest", t.getMessage());
            }
        });
    }

    private void reqProject() {
        Call<HashMap<String, Object>> callReqProject = MainActivity.service.reqProjectAssign(categoryId);
        callReqProject.enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                if(response.isSuccessful()) {
                    getMyProjectList();
                } else {
                    Toast.makeText(getApplicationContext(), "서버와의 통신이 원활하지 않습니다", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "새로운 프로젝트를 할당받을 수 없습니다", Toast.LENGTH_LONG).show();
                Log.d("apiTest", "" + t.getMessage());
            }
        });
    }

    public void onBackPressed(){ /*Back btn 클릭 block*/ }
}