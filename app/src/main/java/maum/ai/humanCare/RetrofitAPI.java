package maum.ai.humanCare;

import java.util.HashMap;

import okhttp3.MultipartBody;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RetrofitAPI {

    @FormUrlEncoded
    @POST("accnt/login.json")
    Call<HashMap<String, Object>> getLoginResult(
            @Field("userId") String userId,
            @Field("passwd") String passwd,
            @Field("localeCd") String localeCd,
            @Field("age") int age,
            @Field("genderCd") String genderCd
    );

    @POST("accnt/logout.json")
    Call<HashMap<String, Object>> getLogoutResult();

    @FormUrlEncoded
    @POST("biz/comm/selectCode.json")
    Call<HashMap<String, Object>> getLocaleCd (@Field("grpCode") String grpCode);

    @FormUrlEncoded
    @POST("accnt/selectAvailableUserList.json")
    Call<HashMap<String, Object>> getUserList(@Field("cheatKey") String cheatKet);

    @POST("accnt/getMyInfo.json")
    Call<HashMap<String, Object>> getUserId();

    @POST("project/selectMyWorkingList.json")
    Call<HashMap<String, Object>> getMyProjectList();

    @FormUrlEncoded
    @POST("biz/comm/selectCode.json")
    Call<HashMap<String, Object>> getProjectCategory(@Field("grpCode") String grpCode);

    @FormUrlEncoded
    @POST("work/findAndStartProject.json")
    Call<HashMap<String, Object>> reqProjectAssign(@Field("categoryId") String categoryId);

    @FormUrlEncoded
    @POST("work/getNextContext.json")
    Call<HashMap<String, Object>> reqNextSentence(@Field("projectId") String projectId);

    @Multipart
    @POST("biz/voice/fileUpload.json")
    Call<HashMap<String, Object>> sendAudio(@Query("projectId") String projectId,
                                            @Query("jobId") String jobId,
                                            @Query("contextId") String contextId,
                                            @Query("playSec") int playSec,
                                            @Part MultipartBody.Part file);
}
