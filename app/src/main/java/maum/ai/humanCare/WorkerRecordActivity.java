package maum.ai.humanCare;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.internal.LinkedTreeMap;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkerRecordActivity extends AppCompatActivity {

    int toggleFlag = 0;

    AlertDialog mDlg_Logout = null;
    AlertDialog.Builder builder;

    String pjtId = "";
    String jobId = "";
    String contextId = "";
    int playSec = 0;

    private final int mBufferSize = 1024;
    private final int mBytesPerElement = 2;

    private final int[] mSampleRates = new int[]{44100, 22050, 11025, 8000};
    private final short[] mAudioFormats = new short[]{AudioFormat.ENCODING_PCM_16BIT, AudioFormat.ENCODING_PCM_8BIT};
    private final short[] mChannelConfigs = new short[]{AudioFormat.CHANNEL_IN_STEREO, AudioFormat.CHANNEL_IN_MONO};

    private int mSampleRate = 44100;
    private short mAudioFormat = AudioFormat.ENCODING_PCM_16BIT;
    private short mChannelConfig = AudioFormat.CHANNEL_IN_MONO;

    private AudioRecord mRecorder = null;
    private Thread mRecordingThread = null;
    private boolean mIsRecording = false;           // 녹음 상태값
    private String mPath = "";                      // 녹음 파일 저장 경로

    private String resultPath = "";

    @BindView(R.id.projectName)
    TextView textview_projectName;

    @BindView(R.id.recordSentence)
    TextView textview_sentence;

    @BindView(R.id.btnMoveStart)
    Button btn_moveStart;

    @BindView(R.id.btnRecordLogout)
    Button btn_recordLogout;

    @BindView(R.id.btnRecord)
    Button btn_record;

    @BindView(R.id.btnReRecord)
    Button btn_reRecord;

    @BindView(R.id.btnSubmit)
    Button btn_submit;

    @BindView(R.id.playfile)
    Button playButton;

    @BindView(R.id.recordResultFrame)
    FrameLayout frame_recordResult;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.worker_record);

        ButterKnife.bind(this);

        builder = new AlertDialog.Builder(this);
        final EditText et = new EditText(this);

        Intent intent = getIntent();
        String thisProjectId = intent.getStringExtra("projectId");
        pjtId = thisProjectId;
        String thisProjectName = intent.getStringExtra("projectName");
        btn_recordLogout.setText( intent.getStringExtra("userId") );
        textview_projectName.setText(" " + thisProjectName);
        frame_recordResult.setVisibility(View.INVISIBLE);

        getSentence(thisProjectId);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playWaveFile();
            }
        });

        btn_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 초기 화면
                if (toggleFlag == 0) {
                    // 녹음 기능
                    startRecording();
                    mIsRecording = true;
                    btn_record.setText("중지");

                    toggleFlag = 1;
                } //녹음 완료 했을 때,
                else if (toggleFlag == 1) {
                    btn_record.setVisibility(View.INVISIBLE);

                    stopRecording();
                    mIsRecording = false;
                    btn_record.setText("녹음");
                    btn_reRecord.setVisibility(View.VISIBLE);
                    btn_submit.setVisibility(View.VISIBLE);

                    toggleFlag = 2;
                }
            }
        });

        btn_reRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_record.setVisibility(View.VISIBLE);
                btn_reRecord.setVisibility(View.INVISIBLE);
                btn_submit.setVisibility(View.INVISIBLE);
                frame_recordResult.setVisibility(View.INVISIBLE);

                toggleFlag = 0;
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textview_sentence.setText("");
                btn_record.setVisibility(View.VISIBLE);
                btn_reRecord.setVisibility(View.INVISIBLE);
                btn_submit.setVisibility(View.INVISIBLE);
                frame_recordResult.setVisibility(View.INVISIBLE);

                sendAudioFile();
                toggleFlag = 0;
            }
        });

        builder = new AlertDialog.Builder(this);
        btn_recordLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                builder.setView(et);
                et.setHint("비밀번호 입력");

                builder.setTitle("로그아웃 확인")
                        .setMessage("로그아웃을 위해 비밀번호를 입력해주세요")
                        .setCancelable(false)
                        .setPositiveButton("로그아웃", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String pwValue = et.getText().toString();

                                if (pwValue.equals("human1234") == true) {
                                    Log.d("apiTest", "------------------------------------------------- 로그인 성공");
                                    //로그아웃 API
                                    Call<HashMap<String, Object>> callLogoutResult = MainActivity.service.getLogoutResult();
                                    callLogoutResult.enqueue(new Callback<HashMap<String, Object>>() {
                                        @Override
                                        public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                                            if (response.isSuccessful()) {
                                                // 로그아웃 성공
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            } else {
                                                Toast.makeText(getApplicationContext(), "서버와의 통신이 원활하지 않습니다", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                                            Log.e("apiTest", t.getMessage());
                                        }
                                    });
                                } else {
                                    Toast.makeText(getApplicationContext(), "비밀번호가 일치하지 않습니다", Toast.LENGTH_LONG).show();
                                    et.setText("");
                                }
                            }
                        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                if (mDlg_Logout == null) mDlg_Logout = builder.create();
                mDlg_Logout.show();
            }
        });

        // '처음으로' 버튼
        btn_moveStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WorkerMainActivity.class);
                startActivity(intent);
            }
        });
    }

    // *******************************************************************************************
    // '제출' 버튼 기능
    // *******************************************************************************************
    private void sendAudioFile() {
        File file = new File(resultPath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("audio/*"), file);
        MultipartBody.Part uploadFile = MultipartBody.Part.createFormData("file", resultPath, requestFile);

        Call<HashMap<String, Object>> callSendAudio = MainActivity.service.sendAudio(pjtId, jobId, contextId, playSec, uploadFile);
        callSendAudio.enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                if(response.isSuccessful()) {
                    Log.d("apiTest", "Upload success : "+response.body().get("success").toString());

                    if(response.body().get("success").toString().equals("true")) {
                        getSentence(pjtId);
                        Toast.makeText(getApplicationContext(), "업로드에 성공하였습니다", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "업로드에 실패하였습니다", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "서버와의 통신이 원활하지 않습니다", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "데이터를 보내지 못 했습니다", Toast.LENGTH_LONG).show();
                Log.e("apiTest", ""+t.getMessage());
            }
        });
    }

    // *******************************************************************************************
    // 문장 가져오기
    // *******************************************************************************************
    private void getSentence(String thisProjectId) {
        Call<HashMap<String, Object>> callSentence = MainActivity.service.reqNextSentence(thisProjectId);
        callSentence.enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                if (response.isSuccessful()) {
                    String sentence = ((LinkedTreeMap<String, Object>) response.body().get("data")).get("context").toString();

                    jobId = ((LinkedTreeMap<String, Object>) response.body().get("data")).get("jobId").toString();
                    contextId = ((LinkedTreeMap<String, Object>) response.body().get("data")).get("contextId").toString();

                    textview_sentence.setText(sentence);
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "문장을 받아올 수 없습니다", Toast.LENGTH_LONG).show();
                Log.d("apiTest", "" + t.getMessage());
            }
        });
    }

    // *******************************************************************************************
    // 녹음 기능
    // *******************************************************************************************
    // 녹음을 위한 Thread 생성 및 실행
    private void startRecording() {
        mRecorder = findAudioRecord();
        mRecorder.startRecording();
        mIsRecording = true;
        mRecordingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                writeAudioDataToFile();
            }
        }, "AudioRecorder Thread");
        mRecordingThread.start();
    }

    // 녹음 환경 설정
    private AudioRecord findAudioRecord() {
        for (int rate : mSampleRates) {
            for (short format : mAudioFormats) {
                for (short channel : mChannelConfigs) {
                    try {
                        int bufferSize = AudioRecord.getMinBufferSize(rate, channel, format);
                        if (bufferSize != AudioRecord.ERROR_BAD_VALUE) {
                            mSampleRate = rate;
                            mAudioFormat = format;
                            mChannelConfig = channel;

                            AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, mSampleRate, mChannelConfig, mAudioFormat, bufferSize);

                            if (recorder.getState() == AudioRecord.STATE_INITIALIZED) {
                                return recorder;    // 설정값으로 생성된 Recorder
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        // 설정값을 찾지 못한 경우
        return null;
    }

    // data file writing
    private void writeAudioDataToFile() {
        String sd = Environment.getExternalStorageDirectory().getAbsolutePath();
        mPath = sd + "/recordFile.pcm";

        short sData[] = new short[mBufferSize];
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(mPath);

            while (mIsRecording) {
                mRecorder.read(sData, 0, mBufferSize);
                byte bData[] = short2byte(sData);
                fos.write(bData, 0, mBufferSize * mBytesPerElement);
            }
            fos.close();

            File f1 = new File(mPath);
            File f2 = new File(mPath.replace(".pcm", ".wav"));
            try {
                rawToWave(f1, f2);
                resultPath = mPath.replace(".pcm", ".wav");

            } catch (IOException e) {
                e.printStackTrace();
            }

            // 파일 시간 계산
            File oFile = new File(resultPath);
            if(oFile.exists()) {
                long L = oFile.length();
                playSec = (int) ((L-44) / 32);
            }

            WorkerRecordActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    frame_recordResult.setVisibility(View.VISIBLE);
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // short array --> byte array
    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;
    }

    private void stopRecording() {
        if (mRecorder != null) {
            mIsRecording = false;
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
            mRecordingThread = null;
        }
    }

    private void playWaveFile() {
        int minBufferSize = AudioTrack.getMinBufferSize(mSampleRate, mChannelConfig, mAudioFormat);
        AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL, mSampleRate, mChannelConfig, mAudioFormat, minBufferSize, AudioTrack.MODE_STREAM);
        int count = 0;
        byte[] data = new byte[mBufferSize];

        try {
            FileInputStream fis = new FileInputStream(mPath);
            DataInputStream dis = new DataInputStream(fis);
            audioTrack.play();
            while ((count = dis.read(data, 0, mBufferSize)) > -1) {
                audioTrack.write(data, 0, count);
            }
            audioTrack.stop();
            audioTrack.release();
            dis.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // *******************************************************************************************
    // 음원 변환 (PCM --> WAV)
    // *******************************************************************************************
    private void rawToWave(final File rawFile, final File waveFile) throws IOException {

        byte[] rawData = new byte[(int) rawFile.length()];
        DataInputStream input = null;
        try {
            input = new DataInputStream(new FileInputStream(rawFile));
            input.read(rawData);
        } finally {
            if (input != null) {
                input.close();
            }
        }

        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));
            // WAVE header
            writeString(output, "RIFF"); // chunk id
            writeInt(output, 36 + rawData.length); // chunk size
            writeString(output, "WAVE"); // format
            writeString(output, "fmt "); // subchunk 1 id
            writeInt(output, 16); // subchunk 1 size
            writeShort(output, (short) 1); // audio format (1 = PCM)
            writeShort(output, (short) 1); // number of channels
            writeInt(output, 44100); // sample rate
            writeInt(output, 1 * 44100 * 16 / 8); // byte rate
            // writeInt(output, RECORDER_SAMPLERATE * 2); // byte rate
            writeShort(output, (short) 2); // block align
            writeShort(output, (short) 16); // bits per sample
            writeString(output, "data"); // subchunk 2 id
            writeInt(output, rawData.length); // subchunk 2 size
            // Audio data (conversion big endian -> little endian)
            short[] shorts = new short[rawData.length / 2];
            ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
            ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
            for (short s : shorts) {
                bytes.putShort(s);
            }

            output.write(fullyReadFileToBytes(rawFile));
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }
    byte[] fullyReadFileToBytes(File f) throws IOException {
        int size = (int) f.length();
        byte bytes[] = new byte[size];
        byte tmpBuff[] = new byte[size];
        FileInputStream fis= new FileInputStream(f);
        try {

            int read = fis.read(bytes, 0, size);
            if (read < size) {
                int remain = size - read;
                while (remain > 0) {
                    read = fis.read(tmpBuff, 0, remain);
                    System.arraycopy(tmpBuff, 0, bytes, size - remain, read);
                    remain -= read;
                }
            }
        }  catch (IOException e){
            throw e;
        } finally {
            fis.close();
        }
        return bytes;
    }
    private void writeInt(final DataOutputStream output, final int value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
        output.write(value >> 16);
        output.write(value >> 24);
    }

    private void writeShort(final DataOutputStream output, final short value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
    }

    private void writeString(final DataOutputStream output, final String value) throws IOException {
        for (int i = 0; i < value.length(); i++) {
            output.write(value.charAt(i));
        }
    }
}