package maum.ai.humanCare;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.internal.LinkedTreeMap;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.CookieJar;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public static Retrofit mRetrofit;
    public static RetrofitAPI service;

    String url_login = "http://125.132.250.204:9995/";

    String genderCd = "0";
    String localeCd = "0";
    int ageCd = 0;

    ArrayAdapter<String> adapter;

    @BindView(R.id.idText)
    EditText id_text;

    @BindView(R.id.pwText)
    EditText pw_text;

    @BindView(R.id.btnLocaleCd)
    Button btn_localeCd;

    @BindView(R.id.ageText)
    EditText age_text;

    @BindView((R.id.male))
    RadioButton genderMale;

    @BindView((R.id.female))
    RadioButton genderFemale;

    @BindView(R.id.btnCheckId)
    Button btn_checkId;

    @BindView(R.id.btnLogin)
    Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setRetrofitInit();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        // ID, PW 창 버튼 클릭 시, 키보드 띄우기
        imm.showSoftInput(id_text, 0);
        imm.showSoftInput(pw_text, 0);

        // 키보드 없애기
        imm.hideSoftInputFromWindow(id_text.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(pw_text.getWindowToken(), 0);

        btn_localeCd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createLocaleList();
            }
        });

        btn_checkId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUserList();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( id_text.getText().toString().length() == 0 ) {
                    Toast.makeText(getApplicationContext(), "아이디를 입력하세요", Toast.LENGTH_LONG).show();
                    return;
                }
                if (pw_text.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), "비밀번호를 입력하세요", Toast.LENGTH_LONG).show();
                    return;
                }

                if(genderMale.isChecked() == true) {
                    genderCd = "1";
                }
                if(genderFemale.isChecked() == true) {
                    genderCd = "2";
                }
                if( !age_text.getText().toString().isEmpty() ) {
                    ageCd = Integer.parseInt(age_text.getText().toString());
                }

                Call<HashMap<String, Object>> call = service.getLoginResult(id_text.getText().toString(), pw_text.getText().toString(), localeCd, ageCd, genderCd);
                call.enqueue(new Callback<HashMap<String, Object>>() {
                    @Override
                    public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                        if(response.isSuccessful()) {
                            if(response.body().get("success").equals(true)) {
                                // 입력창 초기화
                                id_text.setText("");
                                pw_text.setText("");
                                age_text.setText("");
                                btn_localeCd.setText("선택");

                                Intent intent = new Intent(getApplicationContext(), WorkerMainActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "로그인에 실패했습니다", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Log.d("apiTest", "----------------------------------------- 아이디 or 비밀번호 오류");
                            Toast.makeText(getApplicationContext(), "아이디와 비밀번호를 확인해주세요", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                        Log.e("apiTest", t.getMessage());
                        Toast.makeText(getApplicationContext(), "서버와의 통신이 원활하지 않습니다", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    // *******************************************************************************************
    // Retrofit 초기 설정
    // *******************************************************************************************
    private void setRetrofitInit() {
        CookieJar cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(this));
        OkHttpClient client = new OkHttpClient.Builder().cookieJar(cookieJar).build();

        mRetrofit = new Retrofit.Builder().baseUrl(url_login)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create()).build();
        service = mRetrofit.create(RetrofitAPI.class);
    }

    // *******************************************************************************************
    // 버튼 클릭 이벤트 기능
    // *******************************************************************************************
    public void createLocaleList() {

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("지역목록");
        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_expandable_list_item_1);
        alert.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String menu = adapter.getItem(which);
                btn_localeCd.setText(menu);
                localeCd = menu.substring( menu.indexOf("_")+1, menu.length() );
            }
        });

        Call<HashMap<String, Object>> call = service.getLocaleCd("LOCALE_CD");

        call.enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                if(response.isSuccessful()) {
                    List<LinkedTreeMap<String, Object>> listData = (List<LinkedTreeMap<String, Object>>) response.body().get("data");

                    for(int i=0 ; i<listData.size() ; i++) {
                        adapter.add( ""+listData.get(i).get("codeName").toString() + "_"+ listData.get(i).get("code").toString() );
                    }

                    adapter.notifyDataSetChanged();
                    alert.show();
                }
            }
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "지역 코드 목록을 받아올 수 없습니다", Toast.LENGTH_LONG).show();
                Log.d("apiTest", ""+t.getMessage());
            }
        });
    }

    public void createUserList() {

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("사용자목록");
        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_expandable_list_item_1);
        alert.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String menu = adapter.getItem(which);
                id_text.setText(menu);
            }
        });

        Call <HashMap<String, Object>> call = service.getUserList("minds@smt");
        call.enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> call, Response<HashMap<String, Object>> response) {
                if(response.isSuccessful()) {
                    List<LinkedTreeMap<String, Object>> listData = (List<LinkedTreeMap<String, Object>>) response.body().get("data");

                    for(int i=0 ; i<listData.size() ; i++) {
                        adapter.add(""+listData.get(i));
                    }

                    adapter.notifyDataSetChanged();
                    alert.show();
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "계정 목록을 받아올 수 없습니다", Toast.LENGTH_LONG).show();
                Log.d("apiTest", ""+t.getMessage());
            }
        });
    }
}
